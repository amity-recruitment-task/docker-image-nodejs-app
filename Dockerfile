# syntax = docker/dockerfile:1.0-experimental
FROM node:12.18.3-alpine3.12

RUN apk -U upgrade && \
    apk add git npm openssh-client

# prepare SSH environment
RUN mkdir /root/.ssh/
RUN ssh-keyscan -T 60 gitlab.com >> /root/.ssh/known_hosts
# key required to clone repository from Gitlab
RUN --mount=type=secret,id=repokeyfile base64 -d /run/secrets/repokeyfile > /root/.ssh/id_ed25519
RUN chmod 400 /root/.ssh/id_ed25519

COPY docker-entrypoint.sh /usr/local/bin/
ENTRYPOINT ["docker-entrypoint.sh"]