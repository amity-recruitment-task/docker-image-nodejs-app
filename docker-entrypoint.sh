#!/bin/sh

if [ -z "$MONGODB_ADDON_URI" -o -z "$PORT" ]; then
    echo "Set \"MONGODB_ADDON_URI\" and \"PORT\" environment variables"
    exit 1
fi

# Decode the secret link to the database
export MONGODB_ADDON_URI=$(echo "$MONGODB_ADDON_URI" | base64 -d)

PACKAGEDIR="~/demo-nodejs-mongodb-rest"

git clone git@gitlab.com:amity-recruitment-task/demo-nodejs-mongodb-rest.git $PACKAGEDIR

cd $PACKAGEDIR
npm install --no-optional
npm start